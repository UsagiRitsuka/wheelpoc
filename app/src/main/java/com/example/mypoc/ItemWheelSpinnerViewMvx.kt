package com.example.mypoc

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

class ItemWheelSpinnerViewMvx(layoutInflater: LayoutInflater, parent: ViewGroup?) {
    val rootView = layoutInflater.inflate(R.layout.item_wheel_spinner, parent, false)
    private val image: ImageView = rootView.findViewById(R.id.image)

    private val textView: TextView = rootView.findViewById(R.id.text)

    fun test(text: String){
        textView.text = text
    }
}