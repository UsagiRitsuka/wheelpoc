package com.example.mypoc

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.FrameLayout

class MainActivity : AppCompatActivity() {

    private lateinit var bulletScreen: BulletScreenView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val wheelSpinnerViewMvx = WheelSpinnerViewMvx(LayoutInflater.from(this), null)
        bulletScreen = findViewById(R.id.bullet_screen)
        findViewById<FrameLayout>(R.id.frame).addView(wheelSpinnerViewMvx.rootView)
        findViewById<Button>(R.id.spin).setOnClickListener {
//            wheelSpinnerViewMvx.bindDrawResult()
            bulletScreen.add("aaaa")
//            bulletScreen.add("bbbb")
//            bulletScreen.add("CCC")
//            bulletScreen.add("DDDD")
            bulletScreen.add("EEEEEE")
        }




    }
}
