package com.example.mypoc

import android.animation.Animator
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView


class WheelSpinnerViewMvx(layoutInflater: LayoutInflater, parent: ViewGroup?) {

    interface Listener{
        fun onDrawClicked()
    }

    var listener: Listener? = null

    val rootView = layoutInflater.inflate(R.layout.wheel_spinner, parent, false)
    private val frame1: FrameLayout = rootView.findViewById(R.id.frame1)
    private val frame2: FrameLayout = rootView.findViewById(R.id.frame2)
    private val frame3: FrameLayout = rootView.findViewById(R.id.frame3)
    private val frame4: FrameLayout = rootView.findViewById(R.id.frame4)

    private val draw: ImageView = rootView.findViewById(R.id.draw)

    private val itemWheelSpinnerViewMvx1 = ItemWheelSpinnerViewMvx(layoutInflater, parent)
    private val itemWheelSpinnerViewMvx2 = ItemWheelSpinnerViewMvx(layoutInflater, parent)
    private val itemWheelSpinnerViewMvx3 = ItemWheelSpinnerViewMvx(layoutInflater, parent)
    private val itemWheelSpinnerViewMvx4 = ItemWheelSpinnerViewMvx(layoutInflater, parent)


    private lateinit var animator: ObjectAnimator
    init {
        frame1.addView(itemWheelSpinnerViewMvx1.rootView)
        frame2.addView(itemWheelSpinnerViewMvx2.rootView)
        frame3.addView(itemWheelSpinnerViewMvx3.rootView)
        frame4.addView(itemWheelSpinnerViewMvx4.rootView)

        itemWheelSpinnerViewMvx1.test("11")
        itemWheelSpinnerViewMvx2.test("22")
        itemWheelSpinnerViewMvx3.test("33")
        itemWheelSpinnerViewMvx4.test("44")

        frame2.rotation = 90f
        frame3.rotation = 180f
        frame4.rotation = 270f

        draw.setOnClickListener { listener?.onDrawClicked() }
    }

    private fun draw(){
        // 依據target算出停留的角度
        animator = ObjectAnimator.ofFloat(rootView, "rotation", 0f, 3600f)
        animator.duration = 5000
        animator.start()
    }

    fun bindDrawResult(){

        Log.v("Wachi", "duration: ${animator.duration}")

//        animator.setFloatValues((animator.repeatCount + 10) * 360 + 90f)
    }

}